<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitAttribute1 */

$this->title = Yii::t('app', 'Create Unit Attribute1');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unit Attribute1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-attribute1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
