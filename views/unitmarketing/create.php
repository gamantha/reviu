<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitMarketing */

$this->title = Yii::t('app', 'Create Unit Marketing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unit Marketings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-marketing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
