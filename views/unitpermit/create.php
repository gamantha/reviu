<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitPermit */

$this->title = Yii::t('app', 'Create Unit Permit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unit Permits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-permit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
