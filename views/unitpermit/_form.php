<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnitPermit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-permit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'unit_id')->textInput() ?>

    <?= $form->field($model, 'certificate_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'certificate_status')->dropDownList([ 'unsplit' => 'Unsplit', 'split' => 'Split', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'lt_bpn')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
