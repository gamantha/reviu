<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Street]].
 *
 * @see Street
 */
class StreetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Street[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Street|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}