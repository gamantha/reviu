<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_permit".
 *
 * @property integer $id
 * @property integer $unit_id
 * @property string $certificate_no
 * @property string $certificate_status
 * @property integer $lt_bpn
 *
 * @property Unit $unit
 */
class UnitPermit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_permit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_id', 'lt_bpn'], 'integer'],
            [['certificate_status'], 'string'],
            [['certificate_no'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit_id' => Yii::t('app', 'Unit ID'),
            'certificate_no' => Yii::t('app', 'Certificate No'),
            'certificate_status' => Yii::t('app', 'Certificate Status'),
            'lt_bpn' => Yii::t('app', 'Lt Bpn'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    /**
     * @inheritdoc
     * @return UnitPermitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UnitPermitQuery(get_called_class());
    }
}
