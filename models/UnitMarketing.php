<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_marketing".
 *
 * @property integer $id
 * @property integer $unit_id
 * @property integer $customer_id
 * @property integer $price
 * @property string $status
 *
 * @property Customer $customer
 * @property Unit $unit
 */
class UnitMarketing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_marketing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_id', 'customer_id', 'price'], 'integer'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit_id' => Yii::t('app', 'Unit ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    /**
     * @inheritdoc
     * @return UnitMarketingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UnitMarketingQuery(get_called_class());
    }
}
