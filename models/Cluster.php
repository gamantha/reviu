<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cluster".
 *
 * @property integer $id
 * @property string $name
 * @property integer $project_id
 * @property string $meta
 * @property string $status
 *
 * @property Project $project
 * @property Street[] $streets
 * @property Unit[] $units
 */
class Cluster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cluster';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['meta', 'status'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'project_id' => Yii::t('app', 'Project ID'),
            'meta' => Yii::t('app', 'Meta'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreets()
    {
        return $this->hasMany(Street::className(), ['cluster_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['cluster_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ClusterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClusterQuery(get_called_class());
    }
}
