<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnitPermit;

/**
 * UnitPermitSearch represents the model behind the search form about `app\models\UnitPermit`.
 */
class UnitPermitSearch extends UnitPermit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unit_id', 'lt_bpn'], 'integer'],
            [['certificate_no', 'certificate_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnitPermit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'unit_id' => $this->unit_id,
            'lt_bpn' => $this->lt_bpn,
        ]);

        $query->andFilterWhere(['like', 'certificate_no', $this->certificate_no])
            ->andFilterWhere(['like', 'certificate_status', $this->certificate_status]);

        return $dataProvider;
    }
}
