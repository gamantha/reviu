<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UnitMarketing]].
 *
 * @see UnitMarketing
 */
class UnitMarketingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UnitMarketing[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UnitMarketing|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}