<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UnitAttribute1]].
 *
 * @see UnitAttribute1
 */
class UnitAttribute1Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UnitAttribute1[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UnitAttribute1|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}