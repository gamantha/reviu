<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Unit;

/**
 * UnitSearch represents the model behind the search form about `app\models\Unit`.
 */
class UnitSearch extends Unit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cluster_id', 'street_id', 'type_id', 'lt', 'lb'], 'integer'],
            [['meta', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Unit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cluster_id' => $this->cluster_id,
            'street_id' => $this->street_id,
            'type_id' => $this->type_id,
            'lt' => $this->lt,
            'lb' => $this->lb,
        ]);

        $query->andFilterWhere(['like', 'meta', $this->meta])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
