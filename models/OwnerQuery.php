<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Owner]].
 *
 * @see Owner
 */
class OwnerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Owner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Owner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}