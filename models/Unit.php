<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property integer $id
 * @property integer $cluster_id
 * @property integer $street_id
 * @property integer $type_id
 * @property integer $lt
 * @property integer $lb
 * @property string $meta
 * @property string $status
 *
 * @property Cluster $cluster
 * @property Street $street
 * @property Type $type
 * @property UnitAttribute1[] $unitAttribute1s
 * @property UnitMarketing[] $unitMarketings
 * @property UnitPermit[] $unitPermits
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cluster_id', 'street_id', 'type_id', 'lt', 'lb'], 'integer'],
            [['meta', 'status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cluster_id' => Yii::t('app', 'Cluster ID'),
            'street_id' => Yii::t('app', 'Street ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'lt' => Yii::t('app', 'Lt'),
            'lb' => Yii::t('app', 'Lb'),
            'meta' => Yii::t('app', 'Meta'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCluster()
    {
        return $this->hasOne(Cluster::className(), ['id' => 'cluster_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(Street::className(), ['id' => 'street_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitAttribute1s()
    {
        return $this->hasMany(UnitAttribute1::className(), ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitMarketings()
    {
        return $this->hasMany(UnitMarketing::className(), ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitPermits()
    {
        return $this->hasMany(UnitPermit::className(), ['unit_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return UnitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UnitQuery(get_called_class());
    }
}
