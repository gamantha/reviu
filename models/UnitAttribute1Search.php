<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnitAttribute1;

/**
 * UnitAttribute1Search represents the model behind the search form about `app\models\UnitAttribute1`.
 */
class UnitAttribute1Search extends UnitAttribute1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unit_id'], 'integer'],
            [['electricity', 'water'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnitAttribute1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'unit_id' => $this->unit_id,
        ]);

        $query->andFilterWhere(['like', 'electricity', $this->electricity])
            ->andFilterWhere(['like', 'water', $this->water]);

        return $dataProvider;
    }
}
