<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_attribute_1".
 *
 * @property integer $id
 * @property integer $unit_id
 * @property string $electricity
 * @property string $water
 *
 * @property Unit $unit
 */
class UnitAttribute1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_attribute_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_id'], 'integer'],
            [['electricity', 'water'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit_id' => Yii::t('app', 'Unit ID'),
            'electricity' => Yii::t('app', 'Electricity'),
            'water' => Yii::t('app', 'Water'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    /**
     * @inheritdoc
     * @return UnitAttribute1Query the active query used by this AR class.
     */
    public static function find()
    {
        return new UnitAttribute1Query(get_called_class());
    }
}
