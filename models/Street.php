<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "street".
 *
 * @property integer $id
 * @property string $name
 * @property integer $cluster_id
 * @property string $meta
 *
 * @property Cluster $cluster
 * @property Unit[] $units
 */
class Street extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'street';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cluster_id'], 'integer'],
            [['meta'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'cluster_id' => Yii::t('app', 'Cluster ID'),
            'meta' => Yii::t('app', 'Meta'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCluster()
    {
        return $this->hasOne(Cluster::className(), ['id' => 'cluster_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['street_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return StreetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StreetQuery(get_called_class());
    }
}
