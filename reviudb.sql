/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50626
Source Host           : 127.0.0.1:3306
Source Database       : reviudb

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-01-14 21:23:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cluster`
-- ----------------------------
DROP TABLE IF EXISTS `cluster`;
CREATE TABLE `cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `meta` text,
  `status` enum('inactive','active') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cluster_project_1` (`project_id`),
  CONSTRAINT `fk_cluster_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cluster
-- ----------------------------

-- ----------------------------
-- Table structure for `customer`
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customer
-- ----------------------------

-- ----------------------------
-- Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `table` varchar(255) DEFAULT NULL,
  `row_id` int(11) DEFAULT NULL,
  `activity` enum('delete','create','update') DEFAULT NULL,
  `column` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_user_1` (`user_id`),
  CONSTRAINT `fk_log_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log
-- ----------------------------

-- ----------------------------
-- Table structure for `owner`
-- ----------------------------
DROP TABLE IF EXISTS `owner`;
CREATE TABLE `owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `meta` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of owner
-- ----------------------------

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `meta` text,
  `status` enum('inactive','active') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project_owner_1` (`owner_id`),
  CONSTRAINT `fk_project_owner_1` FOREIGN KEY (`owner_id`) REFERENCES `owner` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project
-- ----------------------------

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for `street`
-- ----------------------------
DROP TABLE IF EXISTS `street`;
CREATE TABLE `street` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cluster_id` int(11) DEFAULT NULL,
  `meta` text,
  PRIMARY KEY (`id`),
  KEY `fk_street_cluster_1` (`cluster_id`),
  CONSTRAINT `fk_street_cluster_1` FOREIGN KEY (`cluster_id`) REFERENCES `cluster` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of street
-- ----------------------------

-- ----------------------------
-- Table structure for `type`
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `lt` int(11) DEFAULT NULL,
  `lb` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_type_project_1` (`project_id`),
  CONSTRAINT `fk_type_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of type
-- ----------------------------

-- ----------------------------
-- Table structure for `unit`
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(11) DEFAULT NULL,
  `street_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `lt` int(11) DEFAULT NULL,
  `lb` int(11) DEFAULT NULL,
  `meta` text,
  `status` enum('inactive','active') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unit_cluster_1` (`cluster_id`),
  KEY `fk_unit_street_1` (`street_id`),
  KEY `fk_unit_type_1` (`type_id`),
  CONSTRAINT `fk_unit_cluster_1` FOREIGN KEY (`cluster_id`) REFERENCES `cluster` (`id`),
  CONSTRAINT `fk_unit_street_1` FOREIGN KEY (`street_id`) REFERENCES `street` (`id`),
  CONSTRAINT `fk_unit_type_1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of unit
-- ----------------------------

-- ----------------------------
-- Table structure for `unit_attribute_1`
-- ----------------------------
DROP TABLE IF EXISTS `unit_attribute_1`;
CREATE TABLE `unit_attribute_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) DEFAULT NULL,
  `electricity` enum('no','yes') DEFAULT NULL,
  `water` enum('no','yes') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unit_attribute_1_unit_1` (`unit_id`),
  CONSTRAINT `fk_unit_attribute_1_unit_1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of unit_attribute_1
-- ----------------------------

-- ----------------------------
-- Table structure for `unit_marketing`
-- ----------------------------
DROP TABLE IF EXISTS `unit_marketing`;
CREATE TABLE `unit_marketing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `status` enum('sold','booked','published','ready','processed','planned') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_marketing_unit_1` (`unit_id`),
  KEY `fk_marketing_customer_1` (`customer_id`),
  CONSTRAINT `fk_marketing_customer_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `fk_marketing_unit_1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of unit_marketing
-- ----------------------------

-- ----------------------------
-- Table structure for `unit_permit`
-- ----------------------------
DROP TABLE IF EXISTS `unit_permit`;
CREATE TABLE `unit_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) DEFAULT NULL,
  `certificate_no` varchar(255) DEFAULT NULL,
  `certificate_status` enum('unsplit','split') DEFAULT NULL,
  `lt_bpn` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unit_administration_unit_1` (`unit_id`),
  CONSTRAINT `fk_unit_administration_unit_1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of unit_permit
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` enum('inactive','active') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role_1` (`role_id`),
  CONSTRAINT `fk_user_role_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
